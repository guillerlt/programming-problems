# Platforms
Platform | # solutions
-------- | -----------
Codeforces | 182
LeetCode | 145
CodeChef | 34
Cracking the Coding Interview | 24
Advent of Code | 18
HackerEarth | 17
UVa Online Judge | 15
Project Euler | 4
Kattis | 1
**Total** | 440

# Languages
Language | # solutions
-------- | -----------
C++ | 364
Rust | 74
Python | 1
C | 1
**Total** | 440

